<?php
/*
Plugin Name: Plugin for CompaniesHouseAPI
Version: 2.0
Description: Search Plugin for Companies House. Add the shortcode to the page content ([ch_search])
Author: Alscon (Khalimov Renato)
*/

// If this file is called directly, abort.
//if (!defined('WPINC')) {
//    die;
//}

include_once(dirname(__FILE__) . '/public/admin.php');
include_once(dirname(__FILE__) . '/public/results.php');

class ch_search_shortcode
{
    static $add_script;
    static $add_css;

    static function init()
    {
        add_shortcode('ch_search', array(__CLASS__, 'ch_search_func'));
        add_action('init', array(__CLASS__, 'register_script'));
        add_action('init', array(__CLASS__, 'register_css'));
        add_action('wp_footer', array(__CLASS__, 'print_script'));
    }

    static function ch_search_func($atts)
    {
        self::$add_script = true;
        self::$add_css = true;

        require_once('main.php');
    }

    static function register_script()
    {
        wp_register_script('ch-jquery',  plugins_url('public/js/jquery-2.2.3.min.js', __FILE__));
        wp_register_script('vue-js', plugins_url('public/js/vue.js', __FILE__));
        wp_register_script('main-js', plugins_url('public/js/main.js', __FILE__));
        wp_register_script('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
        wp_register_script('prefix_vue-resource', '//cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.2/vue-resource.js');
        wp_localize_script( 'main-js', 'ch_ajax', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ));

    }

    static function register_css()
    {
        wp_register_style('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
        wp_register_style('ch_search', plugins_url('public/css/main.css', __FILE__));
    }

    static function print_script()
    {
        if (!self::$add_script && !self::$add_css) return;

        //include js
        wp_print_scripts('vue-js');
        wp_print_scripts('main-js');
        wp_print_scripts('ch-jquery');
        wp_enqueue_script('prefix_bootstrap');
        wp_enqueue_script('prefix_vue-resource');

        //include css
//        wp_enqueue_style('prefix_bootstrap');
        wp_enqueue_style('ch_search');
    }

}

ch_search_shortcode::init();

//ch_search_shortcode::search_results();


?>