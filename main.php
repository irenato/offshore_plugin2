<?php clean_post_cache($post->ID); ?>
<?php wp_suspend_cache_addition(true); ?>
<div id="search-app" class="col-xs-12">
    <form class="form-horizontal" method="get" action="#">
        <input type="search" name="val" class="form-control" placeholder="Введите название компании"
               value="<?= isset($_GET['val']) ? $_GET['val'] : '' ?>">
        <input type="submit" value="Поиск">
    </form>
</div>

<?php
if (isset($_GET['val'])) :

    $data = getResults($_GET['val'], (isset($_GET['ch-page']) ? $_GET['ch-page'] - 1 : 0 ));
//    var_dump($data->total_results);
//    die();
    if ($data->total_results > 0) : ?>
        <!--        --><?php //$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2); ?>

        <div class="search-results-list">
            <h2>Результаты поиска</h2>
            <?php $pages = floor($data->total_results / 20) ?>
            <h3>Найдено: <?= $data->total_results ?> </h3>
            <ul>
                <?php
                $i = -1;
                foreach ($data->items as $item) :
                    ++$i; ?>
                    <li <?= ($i == 0 && !$_GET['ch-page']) ? "class=first-item" : '' ?>>
                        <a href="?company_id=<?= $item->company_number ?>"
                           class='search-result-item'><?= $item->title ?></a>
                        <?php if ($item->description) : ?>
                            <p class="title"><?= $item->description ?></p>
                        <?php endif; ?>
                        <?php if ($item->address_snippet) : ?>
                            <p class="title"><?= $item->address_snippet ?></p>
                        <?php endif; ?>
                        <?php if ($item->snippet) : ?>
                            <p class="title">Предидущее имя компании: <?= $item->snippet ?></p>
                        <?php endif; ?>
                    </li>

                <?php endforeach; ?>
            </ul>

            <!--            pagination (start) -->
            <?php if ((int)$data->total_results > 20) : ?>
                <ul class="pagination">

                    <?php if (isset($_GET['ch-page']) && $_GET['ch-page'] > 1) : ?>
                        <li class="arrow-left"><a
                                href="<?= '?val=' . $_GET['val'] . '&ch-page=' . ((int)$_GET['ch-page'] - 1) ?>"></a>
                        </li>
                    <?php endif; ?>

                    <?php $page = (isset($_GET['ch-page']) && (int)$_GET['ch-page'] > 1) ? $_GET['ch-page'] : 1; ?>
                    <?php $page = ($page % 8 == 0) ? $page : (($page - $page % 8) + 1); ?>

                    <?php for ($i = $page; $i <= ($page + 8); $i++) : ?>
                        <?php if (($i * 20) <= (int)$data->total_results - 20) : ?>
                            <?php $current_page = isset($_GET['ch-page']) ? $_GET['ch-page'] : 1; ?>
                            <li class="<?= ($current_page == $i) ? 'active' : '' ?>"><a
                                    href="<?= '?val=' . $_GET['val'] . '&ch-page=' . $i ?>"><?= $i ?></a>
                            </li>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if ((((int)$_GET['ch-page'] + 8) * 20) <= (int)$data->total_results - 20) : ?>
                        <li class="arrow-right"><a
                                href="<?= '?val=' . $_GET['val'] . '&ch-page=' . ((int)$_GET['ch-page'] + 1) ?>"></a>
                        </li>
                    <?php endif; ?>

                </ul>

                <div class="col-xs-12">
                    <form class="form-horizontal pages" method="get" action="#">
                        <input type="hidden" name="val" class="form-control"
                               value="<?= isset($_GET['val']) ? $_GET['val'] : '' ?>">
                        <input type="search" name="ch-page" class="form-control"
                               placeholder="Введите номер страницы: (1 - <?= $pages ?>)">
                        <input type="submit" value="Перейти">
                    </form>
                </div>
            <?php endif; ?>
        </div>
    <?php else: ?>
        <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
    <?php endif; ?>
<?php endif; ?>

<?php if (isset($_GET['company_id'])) : ?>
    <?php $company = searchCompanyDataById($_GET['company_id']); ?>
    <?php $company_history = searchCompanyDataById($_GET['company_id'], 'filing-history'); ?>
    <?php $company_officers = searchCompanyDataById($_GET['company_id'], 'officers'); ?>
    <?php if ($company) : ?>
        <h2 class="company-title title"><?= $company->company_name ?></h2>
        <h4 class="title"> Номер компании: <?= $company->company_number ?></h4>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#block" class="search-result-item" data-toggle="tab">Обзор</a></li>
            <li><a href="#block-2" class="search-history-item" data-toggle="tab">Документы</a></li>
            <li><a href="#block-3" class="search-people-item" data-toggle="tab">Люди</a></li>
        </ul>
        <div class="tab-content" id="ch-plugin-main-block">
            <!--            main (start) -->
            <div class="tab-pane active" id="block">

                <?php if ($company->registered_office_address) : ?>
                    <h4 class='title'>Юридический адрес</h4>
                    <p class="title">
                        <?= $company->registered_office_address->address_line_1 ? $company->registered_office_address->address_line_1 . ', ' : '' ?>
                        <?= $company->registered_office_address->locality ? $company->registered_office_address->locality . ', ' : '' ?>
                        <?= $company->registered_office_address->region ? $company->registered_office_address->region . ', ' : '' ?>
                        <?= $company->registered_office_address->postal_code ? $company->registered_office_address->postal_code . ', ' : '' ?>
                    </p>
                <?php endif; ?>

                <?php if ($company->company_status) : ?>
                    <h4 class='title'>Статус компании</h4>
                    <p class="company-title title">
                        <?= $company->company_status ?>
                        <?= $company->company_status_detail ? " - " . str_replace("-", " ", $company->company_status_detail) : '' ?>
                    </p>
                <?php endif; ?>

                <?php if ($company->type) : ?>
                    <h4 class='title'>Тип компании</h4>
                    <p class="company-title title">
                        <?= str_replace("-", " ", $company->type) ?>
                    </p>
                <?php endif; ?>

                <?php if ($company->date_of_creation) : ?>
                    <h4 class='title'>Дата основания</h4>
                    <p class="company-title title">
                        <?= formattingDate($company->date_of_creation) ?>
                    </p>
                <?php endif; ?>

                <?php if ($company->accounts) : ?>
                    <?php if ($company->accounts->overdue) : ?>
                        <h4 class="title">Отчеты <span class="overdue">ПРОСРОЧЕННЫЕ</span></h4>
                    <?php else : ?>
                        <h4 class="title">Отчеты </h4>
                        <p class="title">Следующий отчетный период с
                            <strong><?= formattingDate($company->accounts->next_made_up_to) ?></strong> по
                            <strong><?= formattingDate($company->accounts->next_due) ?></strong></p>
                    <?php endif; ?>

                    <?php if ($company->accounts->last_accounts) : ?>
                        <p class="title">Последняя дата сдачи финансового отчета
                            <strong><?= formattingDate($company->accounts->last_accounts->made_up_to) ?></strong></p>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($company->confirmation_statement) : ?>
                    <?php if ($company->confirmation_statement->overdue) : ?>
                        <h4 class="title">Ведомость <span class="overdue">ПРОСРОЧЕННАЯ</span></h4>
                    <?php else : ?>
                        <h4 class="title">Ведомость </h4>
                        <p class="title">Следующая дата сдачи ведомости с
                            <strong><?= formattingDate($company->confirmation_statement->next_made_up_to) ?></strong> по
                            <strong><?= formattingDate($company->confirmation_statement->next_due) ?></strong></p>
                    <?php endif; ?>

                    <?php if ($company->confirmation_statement->last_made_up_to) : ?>
                        <p class="title">Последняя дата сдачи ведомости
                            <strong><?= formattingDate($company->confirmation_statement->last_made_up_to) ?></strong>
                        </p>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($company->annual_return) : ?>
                    <?php if ($company->annual_return->overdue) : ?>
                        <h4 class="title">Декларация <span class="overdue">ПРОСРОЧЕННАЯ</span></h4>
                    <?php else : ?>
                        <?php if ($company->annual_return->next_made_up_to && $company->annual_return->next_due) ?>
                            <h4 class="title">Декларация </h4>
                            <p class="title">Следующее декларирование с
                            <strong><?= formattingDate($company->annual_return->next_made_up_to) ?></strong> по
                        <strong><?= formattingDate($company->annual_return->next_due) ?></strong></p>
                    <?php endif; ?>

                    <?php if ($company->accounts->last_accounts) : ?>
                        <p class="title">Последняя дата сдачи финансового отчета
                            <strong><?= formattingDate($company->accounts->last_accounts->made_up_to) ?></strong></p>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($company->sic_codes) : ?>
                    <h4 class="title">Вид деятельности (SIC)</h4>
                    <p class="title"><?= searchDataByCode($company->sic_codes[0]) ?> (<?= $company->sic_codes[0] ?>)</p>
                <?php endif; ?>

                <?php if ($company->previous_company_names) : ?>
                    <h4 class="title">Предидущее имя компании </h4>
                    <?php foreach ($company->previous_company_names as $company_name) : ?>
                        <p class="title"><?= $company_name->name . ' (' . $company_name->effective_from . ' - ' . $company_name->ceased_on . ') ' ?></p>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
            <!--            main (end) -->
            <!--            history (start) -->
            <div class="tab-pane" id="block-2">
                <div class="tr">
                    <div class="th">
                        <p><strong>Дата</strong></p>
                    </div>
                    <div class="th">
                        <p><strong>Описание</strong></p>
                    </div>
                    <div class="th">
                        <p><strong>Просмотреть</strong></p>
                    </div>
                </div>
                <?php if ($company_history) : ?>
                    <?php foreach ($company_history->items as $item) : ?>
                        <div class="tr">
                            <div class="td">
                                <p>
                                    <?= formattingDate($item->date) ?>
                                </p>
                            </div>
                            <div class="td">
                                <?php $desc_details = ''; ?>

                                <?php if ($item->description_values) : ?>
                                    <?php if ($item->description_values->made_up_date) : ?>
                                        <?php $desc_details = ' made up to ' . formattingDate($item->description_values->made_up_date) ?>
                                    <?php elseif ($item->description_values->change_date && $item->description_values->officer_name) : ?>
                                        <?php $desc_details = ' for ' . $item->description_values->officer_name . ' on ' . formattingDate($item->description_values->change_date) ?>
                                    <?php elseif ($item->description_values->officer_name) : ?>
                                        <?php $desc_details = ' of ' . $item->description_values->officer_name . ' as a member ' ?>
                                    <?php endif; ?>
                                    <?php if ($item->description_values->description) : ?>
                                        <p><span
                                                class="document-description"><?= $item->description_values->description ?></span>
                                            <?= $desc_details ?>
                                        </p>
                                    <?php else : ?>
                                        <p><span
                                                class="document-description"><?= str_replace('-', ' ', $item->description) ?></span>
                                            <?= $desc_details ?>
                                        </p>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <p><span
                                            class="document-description"><?= str_replace('-', ' ', $item->description) ?></span>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <div class="td">
                                <p class='download-pdf-now'><a
                                        href="https://beta.companieshouse.gov.uk<?= $item->links->self ?>/document?format=pdf&download=0"
                                        target="_blank" download>открыть PDF</a>( <?= $item->pages ?> pages)</p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
                <?php endif; ?>
            </div>
            <!--            history (end) -->
            <!--            officers (start) -->
            <div class="tab-pane" id="block-3">
                <div class='officers'>
                    <ul class="buttons nav nav-tabs">
                        <li class="active"><a href="#block-people" class="search-result-item"
                                              data-toggle="tab">Люди</a></li>
                        <li><a href="#block-people-ch" class="search-history-item" data-toggle="tab">Люди со
                                значительным контролем</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="block-people">
                            <h3 class="title">Люди </h3>

                            <?php if ($company_officers->total_results) : ?>
                                <h3 class="title"><?= $company_officers->total_results ?> найдено</h3>
                                <?php foreach ($company_officers->items as $item) : ?>
                                    <div class="officer">
                                        <h2 class="title"><?= $item->name ?>
                                            <?php if (!$item->resigned_on) : ?>
                                            <span class="status active">ACTIVE</span></h2>
                                        <?php else: ?>
                                            <span class="status resigned">RESIGNED</span></h2>
                                        <?php endif; ?>
                                        <h4 class='title'>Почтовый адрес</h4>
                                        <?php if ($item->address) : ?>
                                            <p class="title">
                                                <?= $item->address->premises ? $item->address->premises . ', ' : '' ?>
                                                <?= $item->address->address_line_1 ? $item->address->address_line_1 . ', ' : '' ?>
                                                <?= $item->address->address_line_2 ? $item->address->address_line_2 . ', ' : '' ?>
                                                <?= $item->address->locality ? $item->address->locality . ', ' : '' ?>
                                                <?= $item->address->region ? $item->address->region . ', ' : '' ?>
                                                <?= $item->address->country ? $item->address->country . ', ' : '' ?>
                                                <?= $item->address->postal_code ? $item->address->postal_code : '' ?>
                                            </p>
                                        <?php endif; ?>

                                        <ul class='description'>

                                            <?php if ($item->officer_role) : ?>
                                                <li>
                                                    <h4 class="title">Должность</h4>
                                                    <p class="title"> <?= str_replace('-', ' ', $item->officer_role) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->notified_on) : ?>
                                                <li>
                                                    <h4 class="title">Зарегистрирован</h4>
                                                    <p class="title"> <?= formattingDate($item->notified_on) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->date_of_birth) : ?>
                                                <li>
                                                    <h4 class="title">Дата рождения</h4>
                                                    <p class="title"> <?= $item->date_of_birth->month . '.' . $item->date_of_birth->year ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->appointed_on) : ?>
                                                <li>
                                                    <h4 class="title">Назначен</h4>
                                                    <p class="title"> <?= formattingDate($item->appointed_on) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->resigned_on) : ?>
                                                <li>
                                                    <h4 class="title">Подал в отставку</h4>
                                                    <p class="title"> <?= formattingDate($item->resigned_on) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->identification) : ?>

                                                <?php if ($item->identification->identification_type) : ?>
                                                    <?php if ($item->identification->identification_type == 'non-eea') : ?>
                                                        <h5 class="title">Зарегистрирован не в Европейской экономической
                                                            зоне</h5>
                                                        <br>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                                <?php if ($item->identification->legal_authority) : ?>
                                                    <li>
                                                        <h4 class="title">Регулируемый правовыми нормами</h4>
                                                        <p class="title"> <?= $item->identification->legal_authority ?></p>
                                                    </li>
                                                <?php endif; ?>

                                                <?php if ($item->identification->legal_form) : ?>
                                                    <li>
                                                        <h4 class="title">Юридическая форма</h4>
                                                        <p class="title"> <?= $item->identification->legal_form ?></p>
                                                    </li>
                                                <?php endif; ?>

                                                <?php if ($item->identification->place_registered) : ?>
                                                    <li>
                                                        <h4 class="title">Место регистрации</h4>
                                                        <p class="title"> <?= $item->identification->place_registered ?></p>
                                                    </li>
                                                <?php endif; ?>

                                                <?php if ($item->identification->registration_number) : ?>
                                                    <li>
                                                        <h4 class="title">Регистрационный номер</h4>
                                                        <p class="title"> <?= $item->identification->registration_number ?></p>
                                                    </li>
                                                <?php endif; ?>

                                            <?php endif; ?>

                                            <?php if ($item->nationality) : ?>
                                                <li>
                                                    <h4 class="title">Национальность</h4>
                                                    <p class="title"> <?= $item->nationality ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->country_of_residence) : ?>
                                                <li>
                                                    <h4 class="title">Страна проживания</h4>
                                                    <p class="title"> <?= $item->country_of_residence ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->occupation) : ?>
                                                <li>
                                                    <h4 class="title">Профессия</h4>
                                                    <p class="title"> <?= $item->occupation ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->natures_of_control) : ?>
                                                <li>
                                                    <h4 class="title">Характер управления</h4>
                                                    <?php foreach ($item->natures_of_control as $value) ?>
                                                    <p class="people-nc title"> <?= str_replace('-', ' ', $value) ?></p>
                                                </li>
                                            <?php endif; ?>
                                        </ul>

                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
                            <?php endif; ?>
                        </div>
                        <div class="tab-pane" id="block-people-ch">
                            <h3 class="title">Люди со значительным контролем </h3>
                            <?php if ($company_sc_officers->total_results) : ?>
                                <h3 class="title"><?= $company_sc_officers->total_results ?> найдено</h3>
                                <?php foreach ($company_sc_officers->items as $item) : ?>
                                    <div class="officer">
                                        <h2 class="title"><?= $item->name ?>
                                            <?php if (!$item->resigned_on) : ?>
                                            <span class="status active">ACTIVE</span></h2>
                                        <?php else: ?>
                                            <span class="status resigned">RESIGNED</span></h2>
                                        <?php endif; ?>
                                        <h4 class='title'>Почтовый адрес</h4>
                                        <?php if ($item->address) : ?>
                                            <p class="title">
                                                <?= $item->address->premises ? $item->address->premises . ', ' : '' ?>
                                                <?= $item->address->address_line_1 ? $item->address->address_line_1 . ', ' : '' ?>
                                                <?= $item->address->address_line_2 ? $item->address->address_line_2 . ', ' : '' ?>
                                                <?= $item->address->locality ? $item->address->locality . ', ' : '' ?>
                                                <?= $item->address->region ? $item->address->region . ', ' : '' ?>
                                                <?= $item->address->country ? $item->address->country . ', ' : '' ?>
                                                <?= $item->address->postal_code ? $item->address->postal_code : '' ?>
                                            </p>
                                        <?php endif; ?>

                                        <ul class='description'>

                                            <?php if ($item->officer_role) : ?>
                                                <li>
                                                    <h4 class="title">Должность</h4>
                                                    <p class="title"> <?= str_replace('-', ' ', $item->officer_role) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->notified_on) : ?>
                                                <li>
                                                    <h4 class="title">Зарегистрирован</h4>
                                                    <p class="title"> <?= formattingDate($item->notified_on) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->date_of_birth) : ?>
                                                <li>
                                                    <h4 class="title">Дата рождения</h4>
                                                    <p class="title"> <?= $item->date_of_birth->month . '.' . $item->date_of_birth->year ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->appointed_on) : ?>
                                                <li>
                                                    <h4 class="title">Назначен</h4>
                                                    <p class="title"> <?= formattingDate($item->appointed_on) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->resigned_on) : ?>
                                                <li>
                                                    <h4 class="title">Подал в отставку</h4>
                                                    <p class="title"> <?= formattingDate($item->resigned_on) ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->identification) : ?>

                                                <?php if ($item->identification->identification_type) : ?>
                                                    <?php if ($item->identification->identification_type == 'non-eea') : ?>
                                                        <h5 class="title">Зарегистрирован не в Европейской экономической
                                                            зоне</h5>
                                                        <br>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                                <?php if ($item->identification->legal_authority) : ?>
                                                    <li>
                                                        <h4 class="title">Регулируемый правовыми нормами</h4>
                                                        <p class="title"> <?= $item->identification->legal_authority ?></p>
                                                    </li>
                                                <?php endif; ?>

                                                <?php if ($item->identification->legal_form) : ?>
                                                    <li>
                                                        <h4 class="title">Юридическая форма</h4>
                                                        <p class="title"> <?= $item->identification->legal_form ?></p>
                                                    </li>
                                                <?php endif; ?>

                                                <?php if ($item->identification->place_registered) : ?>
                                                    <li>
                                                        <h4 class="title">Место регистрации</h4>
                                                        <p class="title"> <?= $item->identification->place_registered ?></p>
                                                    </li>
                                                <?php endif; ?>

                                                <?php if ($item->identification->registration_number) : ?>
                                                    <li>
                                                        <h4 class="title">Регистрационный номер<</h4>
                                                        <p class="title"> <?= $item->identification->registration_number ?></p>
                                                    </li>
                                                <?php endif; ?>

                                            <?php endif; ?>

                                            <?php if ($item->nationality) : ?>
                                                <li>
                                                    <h4 class="title">Национальность</h4>
                                                    <p class="title"> <?= $item->nationality ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->country_of_residence) : ?>
                                                <li>
                                                    <h4 class="title">Страна проживания</h4>
                                                    <p class="title"> <?= $item->country_of_residence ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->occupation) : ?>
                                                <li>
                                                    <h4 class="title">Профессия</h4>
                                                    <p class="title"> <?= $item->occupation ?></p>
                                                </li>
                                            <?php endif; ?>

                                            <?php if ($item->natures_of_control) : ?>
                                                <li>
                                                    <h4 class="title">Характер управления</h4>
                                                    <?php foreach ($item->natures_of_control as $value) ?>
                                                    <p class="people-nc title"> <?= str_replace('-', ' ', $value) ?></p>
                                                </li>
                                            <?php endif; ?>
                                        </ul>

                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--            officers (end) -->
            <div id="robinzon"><a href='#' OnClick="history.back();" class='button'>Вернуться обратно</a></div>
        </div>
    <?php endif; ?>
<?php endif; ?>
